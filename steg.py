from stegano import lsb, exifHeader
from stegano.lsb import generators

def _lsb( data, file, output=None, mode=None, argv=[]):
    if mode == 'hide':
        hidden = lsb.hide(file, data)
        if output is None:
            raise ValueError("Output file cannot be None.")
        hidden.save(output)
    else:
        return lsb.reveal(file)

def _lsb_with_generators(data, file, gen, output=None, mode=None, argv=[]):
    if len(argv) == 0:
        raise ValueError("Generator name must be specified.")
    if mode=='hide':
        hidden = lsb.hide(file, data, gen)
        if output is None:
            raise ValueError("Output file cannot be None.")
        hidden.save(output)
    else:
        msg = lsb.reveal(file, gen)
        return msg

def _exif_header( data, file, output=None, mode=None, argv=[]):
    assert file.lower().endswith('.jpg') or file.lower().endswith('.jpeg') or file.lower().endswith('.tiff'), 'File format must be JPEG or TIFF.'
    if mode=='hide':
        hidden = exifHeader.hide(file, secret_message=data)
        if output is None:
            raise ValueError("Output file cannot be None.")
        hidden.save(output)
    else:
        msg = exifHeader.reveal(file)
        return msg
            
class Steganer:

    GENERATORS = {
        'carmichael': generators.carmichael(),
        'eratosthenes': generators.eratosthenes(),
        'fermat': generators.fermat(),
        'fibonacci': generators.fibonacci(),
        'log_gen': generators.log_gen(),
        'mersenne': generators.mersenne(),
        'triangular_numbers': generators.triangular_numbers(),
    }

            
    MODES = {'lsb': _lsb, 'lsbWithGenerators':_lsb_with_generators, 'exifHeader': _exif_header}
        
    def lsb(self, data, file, output=None, mode=None, argv=[]):
        return _lsb(data, file, output, mode, argv)

    def lsb_with_generators(self, data, file, output=None, mode=None, argv=[]):
        if len(argv) == 0:
            raise ValueError("Generator name must be specified.")
        assert gen in self.GENERATORS.keys()
        return _lsb_with_generators(data, file, gen, output, mode, argv)

    def exif_header(self, data, file, output=None, mode=None, argv=[]):
        return _exif_header(data, file, output, mode, argv)
                        
    def hide(self, data, file, option='lsb', output=None, argv=[]):
        assert option in self.MODES.keys()
        if output is None:
            output = file
        self.MODES[option](data, file, output, mode='hide', argv=argv)
            
    def reveal(self, file, option='lsb', argv=[]):
        assert option in self.MODES.keys()
        msg = self.MODES[option]('', file, mode='reveal', argv=argv)
        return msg
