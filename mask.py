class Mask:
    FORMATS = {
        'png': {'start_bytes': b'\x89\x50\x4e\x47\x0d\x0a\x1a\x0A\x00\x00\x00\x0d\x49\x48\x44\x52\x00\x00\x00\x01\x00\x00\x00\x01\x08\x02\x00\x00\x00\x90\x77\x53\xde\x00\x00\x00\x0c\x49\x44\x41\x54\x08\xd7\x63\xf8\xcf\xc0\x00\x00\x03\x01\x01\x00\x18\xdd\x8d\xb0', 'end_bytes': b'\x00\x00\x00\x00\x49\x45\x4e\x44\xae\x42\x60\x82'},
        'jpg': {'start_bytes': b'\xff\xd8\xff', 'end_bytes': b''},
        'gif': {'start_bytes': b'GIF87a', 'end_bytes': b'\x3b'},
        'zip': {'start_bytes': b'PK\x03\x04', 'end_bytes': b'\x50\x4B\x05\x06\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'},
        'rar': {'start_bytes': b'\x52\x61\x72\x21\x1a\x07\x00', 'end_bytes': b''},
        'mp3': {'start_bytes': b'\x49\x44\x33', 'end_bytes': b''},
        'mp4': {'start_bytes': b'\x66\x74\x79\x70\x4D\x53\x4E\x56', 'end_bytes': b''},
        'xml': {'start_bytes': b'<?xml version="1.0"?>\n<', 'end_bytes': b'/>\n</Workbook>'},
        'tif': {'start_bytes': b'\x49\x49\x2a\x00', 'end_bytes': b''},
        'elf': {'start_bytes': b'\x7f\x45\x4c\x46', 'end_bytes': b''},
        'exe': {'start_bytes': b'\x4d\x5a', 'end_bytes': b''},
        'wav':  {'start_bytes': b'\x52\x49\x46\x46', 'end_bytes': b'\x57\x41\x56\x45'},        
    }

    def __init__(self, fmt):
        assert fmt in self.FORMATS.keys() or fmt is None
        self.fmt = fmt
        
    def mask(self, data):
        if self.fmt is None:
            return data
        return self.FORMATS[self.fmt]['start_bytes'] + data + self.FORMATS[self.fmt]['end_bytes']

    def unmask(self, data):
        if self.fmt is None:
            return data
        data = data[ len(self.FORMATS[self.fmt]['start_bytes']) : len(data) - len(self.FORMATS[self.fmt]['end_bytes']) ]
        return data       
