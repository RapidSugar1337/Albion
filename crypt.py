from Crypto.Cipher import ChaCha20, ChaCha20_Poly1305, Salsa20
from Crypto.Util.number import *
from Crypto.Util.Padding import pad, unpad
from Crypto.Hash import SHA256, SHA384, SHA512
from Crypto.Hash import SHA3_224, SHA3_256, SHA3_384, SHA3_512

from camellia import CamelliaCipher
import camellia


class Cryptor:
    
    def __init__(self, cipher, key, hash_=None, iv=None, mode=None):
        self.CIPHERS = {
               'chacha20poly': self.chacha20poly, 
               'chacha20': self.chacha20, 
               'salsa20': self.salsa20, 
               'camellia': self.camellia
               }
               
        self.HASHES = {'sha256': SHA256, 'sha384': SHA384, 'sha512': SHA512,
        'sha3-224': SHA3_224, 'sha3-256': SHA3_256, 'sha3-384': SHA384, 'sha3-512': SHA3_512}
    
        assert cipher.lower() in self.CIPHERS.keys(), f'Unknown cipher: {cipher}'
        assert (cipher == 'chacha20poly' and hash_==None) or hash_.lower() in self.HASHES.keys(), f'Unknown cipher: {hash_}'

        self.cipher = cipher.lower()
        self.hash_ = hash_
        self.key = key
        self.iv = iv
        self.mode = self.get_mode(mode)

    def get_mode(self, mode):
        if mode is None:
            return mode
            
        if mode.lower() == 'ecb' and self.cipher == 'camellia':
            return camellia.MODE_ECB
        elif mode.lower() == 'cbc' and self.cipher == 'camellia':
            return camellia.MODE_CBC
        elif mode.lower() == 'cfb' and self.cipher == 'camellia':
            return camellia.MODE_CFB
        elif mode.lower() == 'ofb' and self.cipher == 'camellia':
            return camellia.MODE_OFB
            
        # TODO: add CTR mode for camellia cipher.
        #elif mode.lower() == 'ctr' and self.cipher == 'camellia':
        #    return camellia.MODE_CTR
        else:
            raise ValueError("Unknown cipher mode")

    def companate(self, ct, hsh):
        return hsh+ct

    def uncompanate(self, data):
        if self.cipher != 'chacha20poly':
            test = self.get_signature(b'\x00')
        else:
            test = b'\x00' * 16
        
        tag = data[:len(test)]
        ciphertext = data[len(test):]
        
        return ciphertext, tag

    def get_signature(self, data):
        signature = self.HASHES[self.hash_].new(data=self.key+data).digest()     
        return signature

    def verify(self, data, signature):
        s = self.get_signature(data)
        return s == signature
        
    def chacha20poly(self, data, mode):
        cipher = ChaCha20_Poly1305.new(key=self.key, nonce=self.iv)
        if mode == 'encrypt_and_sign':
            ciphertext, tag = cipher.encrypt_and_digest(data)
            res = self.companate(ciphertext, tag)
        else:
            ciphertext, tag = self.uncompanate(data)
            res = cipher.decrypt_and_verify(ciphertext, tag)
        return res
        
    
    def chacha20(self, data, mode):
        cipher = ChaCha20.new(key=self.key, nonce=self.iv)
        if mode == 'encrypt_and_sign':
            signature = self.get_signature(data)
            res = self.companate( cipher.encrypt(data), signature )
            return res
        else:
            res, signature = self.uncompanate( data )
            res = cipher.decrypt(res)
            if self.verify(res, signature):
                return res
            else:
                 print("Unable to verify data.")       

    def salsa20(self, data, mode):
        cipher = Salsa20.new(self.key, nonce=self.iv)
        if mode == 'encrypt_and_sign':
            signature = self.get_signature(data)
            res = self.companate( cipher.encrypt(data), signature )
            return res
        else:
            res, signature = self.uncompanate( data )
            res = cipher.decrypt(res)
            if self.verify(res, signature):
                return res
        
    def camellia(self, data, mode):
        cipher = CamelliaCipher(key=self.key, IV = self.iv, mode = self.mode)
        if mode == 'encrypt_and_sign':
            signature = self.get_signature(data)
            if self.mode in [camellia.MODE_ECB, camellia.MODE_CBC, camellia.MODE_CFB]:
                data = pad(data, 16)
            #print(data)
            res = self.companate( cipher.encrypt(data), signature )
            #print(res)
            return res
        else:
            res, signature = self.uncompanate( data )
            res = cipher.decrypt(res)

            if self.mode in [camellia.MODE_ECB, camellia.MODE_CBC, camellia.MODE_CFB]:
                data = pad(data, 16)
                try:
                    res = unpad(res, 16)
                except Exception as e:
                    pass
                    
            if self.verify(res, signature):
                return res
                
        
    def encrypt(self, data):
        ciphertext = self.CIPHERS[self.cipher](data, 'encrypt_and_sign')
        return ciphertext

    def decrypt(self, data):
        plaintext = self.CIPHERS[self.cipher](data, 'decrypt_and_verify')
        return plaintext
