# Albion

Simple tool for encryption/decryption and safe removal of information.
It was made for practicing programming skills and spending time with joy.
It's also my way to make world a little bit better :)

Albion supports relatively strong ciphers like Camellia, ChaCha20 and Salsa20 (at least, as far as I know there is no way to crack them nowdays, if I'm wrong, please
tell me how to do that). You can sign files using SHA256 or SHA512 algorithms, or optionally use ChaCha20_Poly1395 to check that information was not changed.

# Why?
Yes, I know there are a lot of more feature-full tools for messing with files. I wrote this program just because I wanted to create something.
No matter what, just interesting and useful, somehow connected with privacy.

# Installation:
	git clone https://gitea.com/RapidSugar1337/Albion
    pip3 install pycryptodome colorama pwntools stegano
	cd Albion

# Usage:
	python3 albion.py -h # lists all options, play with it a little bit

# Future plans (TO DO):
	* Add more encryption algorithms
    * Add steganography option
	* Create GUI interface (should I...?)