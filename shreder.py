import os

class Shreder:
    def __init__(self, iterations, size=None, removeAfter=False, exact=False, zeros=False):
        self.iterations = iterations
        self.size = size
        self.removeAfter = removeAfter
        self.exact = exact
        self.zeros = zeros

    def get_command(self):
        command = 'shred '
        if self.iterations:
            command += f'--iterations={self.iterations} '
        if not( self.size is None ):
            command += f'--size={self.size} '
        if self.exact:
            command += f'--exact '
        if self.zeros:
            command += f'-z '
        if self.removeAfter:
            command += f'-u '
        return command
        
    def shred(self, filename, ask_password, fail_info):
        # ask_password - function, that asks user for password, cause 
        #                root privileges are needed for safely deleting files.
        # fail_info - function for showing error messages to user.
        
        if os.uname().sysname=='Linux':
            password = ask_password()
            command = f'echo {password} | sudo -S ' + self.get_command() + filename
            #print('#DEBUG: command =', command)
            os.system(command)
        else:
            fail_info('Sorry, this option is not available on your OS yet.')
