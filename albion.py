import argparse
from getpass import getpass
from base64 import b64decode
from os import urandom
from pwn import log # for success and failures logs

from random import choice

#from steg import *
from crypt import *
from mask import *
from shreder import *

from colorama import Fore
# prints fancy banner
def banner():
    ban0 = '''
          __      ___       _______   __      ______    _____  ___   
         /""\\    |"  |     |   _  "\\ |" \\    /    " \\  (\\"   \\|"  \\  
        /    \\   ||  |     (. |_)  :)||  |  // ____  \\ |.\\\\   \\    | 
       /' /\\  \\  |:  |     |:     \\/ |:  | /  /    ) :)|: \\.   \\\\  | 
      //  __'  \\  \\  |___  (|  _  \\\\ |.  |(: (____/ // |.  \\    \\. | 
     /   /  \\\\  \\( \\_|:  \\ |: |_)  :)/\\  |\\\\        /  |    \\    \\ | 
    (___/    \\___)\\_______)(_______/(__\\_|_)\\"_____/    \\___|\\____\\) 
    '''
    ban1 = '''
              ___   ___                                
             (   ) (   )      .-.                      
      .---.   | |   | |.-.   ( __)   .--.    ___ .-.   
     / .-, \  | |   | /   \  (''")  /    \  (   )   \  
    (__) ; |  | |   |  .-. |  | |  |  .-. ;  |  .-. .  
      .'`  |  | |   | |  | |  | |  | |  | |  | |  | |  
     / .'| |  | |   | |  | |  | |  | |  | |  | |  | |  
    | /  | |  | |   | |  | |  | |  | |  | |  | |  | |  
    ; |  ; |  | |   | '  | |  | |  | '  | |  | |  | |  
    ' `-'  |  | |   ' `-' ;   | |  '  `-' /  | |  | |  
    `.__.'_. (___)   `.__.   (___)  `.__.'  (___)(___)
    '''
    ban2 = '''
    ___       ___       ___       ___       ___       ___   
   /\\  \\     /\\__\\     /\\  \\     /\\  \\     /\\  \\     /\\__\\  
  /::\\  \\   /:/  /    /::\\  \\   _\\:\\  \\   /::\\  \\   /:| _|_ 
 /::\\:\\__\\ /:/__/    /::\\:\\__\\ /\\/::\\__\\ /:/\\:\\__\\ /::|/\\__\\
 \\/\\::/  / \\:\\  \\    \\:\\::/  / \\::/\\/__/ \\:\\/:/  / \\/|::/  /
   /:/  /   \:\\__\\    \\::/  /   \\:\\__\\    \\::/  /    |:/  / 
   \\/__/     \\/__/     \\/__/     \\/__/     \\/__/     \\/__/  
                                                              
    '''
    clrs = [Fore.RED, Fore.YELLOW, Fore.GREEN, Fore.CYAN, Fore.BLUE, Fore.MAGENTA]
    print( choice(clrs), choice( [ban0, ban1, ban2] ), Fore.RESET )

def get_parser():
    parser = argparse.ArgumentParser(
        prog='Albion',
        description='Simple encryption and shreding tool.',
        epilog='You have something to hide even if you are not a criminal.'
    )
    
    parser.add_argument('-a', '--hash', help = 'Hash algorithm to sign or verify file.')
    parser.add_argument('-c', '--cipher', help = 'Cipher to encrypt file.')
    parser.add_argument('-d', '--decrypt', action='store_true')
    parser.add_argument('-e', '--encrypt', action='store_true')
    parser.add_argument('-f', '--file', help='Target file.')
    parser.add_argument('-g', '--generate', help='Use it to generate key and nonce files.', action='store_true')
    parser.add_argument('-i', '--iterations', help='Number of iterations. Works only for --shred option.')
    parser.add_argument('-k', '--key', help = 'File with key bytes. May specify a key for usage or an output file for key.')
    parser.add_argument('-k64', '--key64', help = 'The same as -k but instead of file you must specify base64 encoded key.')
    parser.add_argument('-l', '--mode', help = 'Cipher mode (ECB, CBC, CFB, OFB). Only if you are using Camellia or AES for encryption.')
    parser.add_argument('-m', '--mask', help = 'Mask a file like it has particular format (png, zip, odt, etc.)')
    parser.add_argument('-n', '--nonce', help = 'IV or nonce for cipher. Must be a file with bytes. May specify a nonce for usage or an output file for nonce.')
    parser.add_argument('-n64', '--nonce64', help = 'The same as -n but instead of file you must specify base64 encoded IV/nonce.')
    parser.add_argument('-o', '--output', help = 'Output file. Use for creating encrypted copyies of file.')    
    parser.add_argument('-s', '--shred', help = 'Use shred command to safely delete file. Requires using arguments -i and optionally -z, -x, -w, -u.', action='store_true')
    parser.add_argument('-u', '--unlink', help = 'Flag for shred command.', action='store_true')
    parser.add_argument('-w', '--size', help='Flag for shred command.')
    parser.add_argument('-x', '--exact', help='Flag for shred command.', action='store_true')
    parser.add_argument('-z', '--zero', help='Flag for shred command.', action='store_true')
    return parser

def ask_password():
    password = getpass('Shreding requires root privileges. Please enter password to continue: ')
    return password
    
def fail_info(msg):
    log.error(msg)

def genkey(cipher, output_key='key.key', output_nonce='nonce.or.iv'):
    # Ciphers and their key lengths.
    ciphers = {
                   'chacha20poly': [32, 8], 
                   'chacha20': [32, 4], 
                   'salsa20': [32, 8], 
                   'camellia': [16, 16],
            }
    assert cipher in ciphers.keys(), 'Unknown cipher'
    key_length, nonce_length = ciphers[cipher]
    key = urandom(key_length)
    nonce = urandom(nonce_length)
    
    with open(output_key, 'wb') as f:
        f.write(key)
    with open(output_nonce, 'wb') as f:
        f.write(nonce)
    log.success(f'Done. Your key in "{output_key}", nonce in "{output_nonce}".')
    
def main(args):
    # Key generation, if specified
    if args.generate:
        cipher = args.cipher
        if cipher is None:
            print('You must specify a cipher.')
            return
        if not( args.key is None ) and not(args.nonce is None):
            genkey(cipher, args.key, args.nonce)
        elif args.key is None and args.nonce is None:
            genkey(cipher)
        elif args.key is None:
            genkey(cipher, output_nonce = args.nonce)
        elif args.nonce is None:
            genkey(cipher, output_key=args.key)
            
        return
            
    # Cryptography mode
    if not(args.cipher is None):
        cipher = args.cipher
        if not(args.hash is None):
            hash_ = args.hash
        else:
            if cipher.lower() != 'chacha20poly':
                log.error('You must specify hash algo to safely encrypt file. Use sha256 or sha512, for example.')
            else:
                hash_ = None                
        if args.key is None and not(args.key64 is None):
            key = b64decode( args.key64 )
        elif not(args.key is None):
            key = open(args.key, 'rb').read()
        else:
            log.error('Sorry, dude, you need to specify key. Try using -g option if you don\'t know where to take it.')
            
        if not(args.nonce is None):
            nonce = open(args.nonce, 'rb').read()
        elif not(args.nonce64 is None):
            nonce = b64decode( args.nonce64 )
        else:
            log.warning('You did not specified nonce. File may be incorrectly/unsafely encrypted/decrypted. ')
            option = input('Do you want to continue? [y/n]').lower()[0]
            if option=='n':
                return
            nonce = None
        
        mode = args.mode
            
        cryptor = Cryptor(cipher, key, hash_, nonce, mode)

        with open(args.file, 'rb') as f:
            data = f.read()

        m = Mask(args.mask)
        
        if args.encrypt:
            output = cryptor.encrypt(data)
            result = m.mask(output)
            
        elif args.decrypt:
            data = m.unmask(data)
            result = cryptor.decrypt(data)
        
        if args.output is None:
            with open(args.file, 'wb') as f:
                f.write(result)
        else:
            with open(args.output, 'wb') as f:
                f.write(result)
        log.success('Done')
    # Shreding mode
    else:
        if args.shred:
            delete = args.unlink
            size = args.size
            exact = args.exact
            zero = args.zero
            iters = args.iterations
            sh = Shreder(iters, size, delete, exact, zero)
            sh.shred(args.file, ask_password, fail_info)
            log.success('Done')
        else:
            print('This program does not understand you. Please specify flag -h and read instructions, then use them to do what you want to do.')
            return

if __name__=='__main__':
    banner()
    p = get_parser()
    main(p.parse_args())
